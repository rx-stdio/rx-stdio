import { makeRxStdin } from '../src/RxStdin';
import { makeRxStdout } from '../src/RxStdout';

console.log("Please enter some text:");

makeRxStdin()
    .map((input: string) => "You entered: " + input)
    .subscribe(makeRxStdout());
