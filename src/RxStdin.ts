import * as Rl from 'readline';
import * as Rx from '@reactivex/rxjs';

export function makeRxStdin(): Rx.Observable<string> {
    return Rx.Observable.fromEvent(Rl.createInterface({
        input: process.stdin
    }), 'line')
}

