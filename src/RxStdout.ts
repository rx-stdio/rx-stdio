import * as Rx from '@reactivex/rxjs';

function next (value: any) { console.log(value); }
function error (err: any) { console.error(err); }
function complete () {}

export function makeRxStdout(): Rx.Subscriber<any> {
    return Rx.Subscriber.create(next, error, complete);
}
