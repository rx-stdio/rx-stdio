This module wraps Node's stdin and stdout into the framework of RxJS5.
It was developed using TypeScript 2.3.4.

It defines 2 functions:
* makeRxStdin(), which returns an Observable<string> yielding the lines read from process.stdin
* makeRxStdout(), which returns a Subscriber<any> writing what it gets to process.stdout, using console.log()

Usage (see also the included test):
`makeRxStdin().subscribe(makeRxStdout());`

Build (if necessary): `tsc`

